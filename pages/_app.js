import Header from "../components/Header";
import Head from "next/head";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
          crossorigin="anonymous"
        />
        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
          crossorigin="anonymous"
        ></script>
        <script
          defer
          src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"
        ></script>
        <title>Country Next App</title>
      </Head>
      <Header />
      <div className="col-md-12 text-center pb-4">
        <h1 className="display-4 mx-auto px-5">
          Take A Look At The Various Countries in the World
        </h1>
      </div>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
