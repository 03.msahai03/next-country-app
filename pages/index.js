import Link from "next/link";

export default function Home() {
  return (
    <div>
      <div className="container">
        <div className="row justify-content-center">
          <hr />
          <div className="col-md-3 text-center mt-5">
            <Link href="/login">
              <div className="btn btn-lg btn-dark mt-5"> Log In</div>
            </Link>
          </div>
          <div className="col-md-3 text-center mt-5">
            <Link href="/register">
              <div className="btn btn-lg btn-dark mt-5"> Sign Up</div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
