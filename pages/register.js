import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup.object().shape({
  name: yup.string().max(20).required("Name is required"),
  email: yup.string().email().required("Email is required"),
  password: yup.string().min(4).max(15).required(),
  confirmPassword: yup.string().oneOf([yup.ref("password"), null]),
});

const register = () => {
  const router = useRouter();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    console.log(data);
    router.push("/login");
  };

  return (
    <div className="bg my-5 py-5">
      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card card-signin flex-row my-5">
            <div className="card-body">
              <h5 className="card-title text-center">Create An Account</h5>
              <hr />
              <form
                noValidate
                onSubmit={handleSubmit(onSubmit)}
                className="form-signin"
              >
                <div className="form-label-group">
                  <input
                    {...register("name")}
                    type="text"
                    placeholder="Name"
                    name="name"
                    className="form-control form-control-lg"
                    id="name"
                    autoFocus
                  />
                  <label htmlFor="name">Name</label>
                  <p className="fs-6 fw-light ">{errors.name?.message}</p>
                </div>

                <div className="form-label-group">
                  <input
                    {...register("email")}
                    type="email"
                    id="email"
                    name="email"
                    className="form-control form-control-lg"
                    placeholder="Email address"
                  />
                  <label htmlFor="email">Email address</label>
                  <p className="fs-6 fw-light ">{errors.email?.message}</p>
                </div>

                <div className="form-label-group">
                  <input
                    {...register("password")}
                    type="password"
                    id="password"
                    name="password"
                    className="form-control form-control-lg"
                    placeholder="Password"
                  />
                  <label htmlFor="password">Password</label>
                  <p className="fs-6 fw-light ">{errors.password?.message}</p>
                </div>

                <div className="form-label-group">
                  <input
                    {...register("confirmPassword")}
                    type="password"
                    name="confirmPassword"
                    id="confirmPassword"
                    className="form-control form-control-lg"
                    placeholder="Password"
                  />
                  <label htmlFor="confirmPassword">Confirm Password</label>
                  <p> {errors.confirmPassword && "Passwords Should Match!"} </p>
                </div>
                <hr />
                <div className="container d-flex">
                  <button
                    className="btn btn-lg btn-primary btn-block text-uppercase font-weight-bold mb-2 mx-auto"
                    type="submit"
                  >
                    Sign Up
                  </button>
                </div>
              </form>
              <div className="mt-3 mb-3 text-center">
                <Link className="small" href="/login">
                  Already have an account? Log in
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default register;
