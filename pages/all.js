import Countries from "../components/Countries";
import Search from "../components/Search";

const all = () => {
  return (
    <div>
      <Search />
      <Countries />
    </div>
  );
};

export default all;
