import { useRouter } from "next/router";
import fetch from "node-fetch";
import Link from "next/link";
import React from "react";

export default function Country({ country, borders }) {
  const router = useRouter();
  // const { alpha3Code } = router.query;

  let currencies = [];
  let languages = [];

  if (country) {
    for (const currency of country.currencies) {
      currencies.push(currency.name);
    }
    for (const language of country.languages) {
      languages.push(language.name);
    }
  }

  const name = country.name;
  const lat = country.latlng[0];
  const lng = country.latlng[1];

  return (
    <main>
      <div className="country-detail container">
        <div className="col-md-12">
          {/* <Link href="/all">
                <a className="btn shadow my-5 link">Back</a>
              </Link> */}
          <h1 className="display-3 text-center my-5">{country.name}</h1>
          <div className="d-flex container justify-content-between">
            <img
              className="shadow mb-5 col-md-4 col-sm-6 flagImg"
              src={country.flag}
              alt={country.name + " flag"}
            />
            <div className="col-md-3 text-center">
              <a
                href={
                  "https://www.google.com/maps/place/" +
                  name +
                  "/@" +
                  lat +
                  "," +
                  lng +
                  "," +
                  "5z"
                }
                target="_blank"
              >
                <div className="btn btn-lg btn-dark">Show Map</div>
              </a>
            </div>
          </div>
        </div>
        <div className="col-md-12">
          <div className="container mb-5">
            <ul>
              <li>
                <strong>Native name: </strong> {country.nativeName}
              </li>
              <li>
                <strong>Population: </strong>{" "}
                {Number(country.population).toLocaleString("en")}
              </li>
              <li>
                <strong>Region: </strong> {country.region}, {country.subregion}
              </li>
              <li>
                <strong>Capital: </strong> {country.capital}
              </li>
            </ul>
            <ul>
              <li>
                <strong>Currencies:</strong> {currencies.join(", ")}
              </li>
              <li>
                <strong>Languages:</strong> {languages.join(", ")}
              </li>
            </ul>
          </div>

          <h2 className="mb-3">Border Countries: </h2>
          <div className="buttons-container d-flex">
            <div className="row">
              {borders.map((border, index) => {
                const code = border.alpha3Code;
                return (
                  <Link
                    key={index}
                    as={`/country/${code}`}
                    href="/country/[code]"
                  >
                    <div className="cardC card link col-md-4 mx-auto m-3">
                      <img src={border.flag} className="borderImg" />
                      <a className="text-dark text-wrap">{border.name}</a>
                    </div>
                  </Link>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export async function getStaticPaths() {
  const res = await fetch("https://restcountries.eu/rest/v2/all");
  const countries = await res.json();

  return {
    paths: countries.map((country) => `/country/${country.alpha3Code}`) || [],
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const res = await fetch(
    "https://restcountries.eu/rest/v2/alpha/" + params.code
  );
  const country = await res.json();

  let query = [];
  for (const border of country.borders) {
    query.push(border);
  }
  const resN = await fetch(
    "https://restcountries.eu/rest/v2/alpha?codes=" + query.join(";")
  );
  const data = await resN.json();
  let borders = [];
  if (data.length > 0) {
    for (let border of data) {
      borders.push({
        parent: params.code,
        alpha3Code: border.alpha3Code,
        name: border.name,
        flag: border.flag,
      });
    }
  }
  return {
    props: {
      country,
      borders,
    },
  };
}
