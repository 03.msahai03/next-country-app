import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

const schema = yup.object().shape({
  email: yup.string().email().required("Email is required"),
  password: yup.string().required("Password is required"),
});

const login = () => {
  const router = useRouter();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onFormSubmit = (data) => {
    console.log(data);
    router.push("/all");
  };

  return (
    <div className="bg container py-2">
      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
          <div className="card card-signin my-5 shadow">
            <div className="card-body">
              <h4 className="card-title text-center">Welcome back!</h4>
              <hr />
              <form
                onSubmit={handleSubmit(onFormSubmit)}
                className="form-signin"
                noValidate
              >
                <div className="form-label-group">
                  <input
                    {...register("email")}
                    type="email"
                    name="email"
                    id="email"
                    className="form-control form-control-lg"
                    placeholder="Email address"
                    autoFocus
                  />
                  <label htmlFor="email">Email address</label>
                  <p className="fs-6 fw-light ">{errors.email?.message}</p>
                </div>
                <div className="form-label-group">
                  <input
                    {...register("password")}
                    type="password"
                    name="password"
                    id="password"
                    className="form-control form-control-lg"
                    placeholder="Password"
                  />
                  <label htmlFor="password">Password</label>
                  {errors.password && (
                    <p className="fs-6 fw-light">{errors.password?.message}</p>
                  )}
                </div>
                <hr />
                <div className="container d-flex">
                  <button
                    className="btn btn-lg btn-primary btn-block text-uppercase font-weight-bold mb-2 mx-auto"
                    type="submit"
                  >
                    Log in
                  </button>
                </div>
              </form>
              <div className="mt-3 mb-3 text-center">
                <Link className="small" href="/register">
                  Don't have an account? Sign Up
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default login;
