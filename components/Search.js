import React from "react";

const Search = () => {
  if (typeof window !== "undefined") {
    window.addEventListener("DOMContentLoaded", () => {
      const search = document.getElementById("search");

      search.addEventListener("input", (e) => {
        const { value } = e.target;

        const countryName = document.querySelectorAll(".country-names");

        countryName.forEach((name) => {
          if (name.innerText.toLowerCase().includes(value.toLowerCase())) {
            name.parentElement.parentElement.style.display = "block";
          } else {
            name.parentElement.parentElement.style.display = "none";
          }
        });
      });
    });
  }

  return (
    <div className="container">
      <div className="form-group col-md-4 my-5">
        <input
          type="search"
          id="search"
          placeholder="Search for a country"
          className="form-control form-control-lg"
        />
      </div>
    </div>
  );
};

export default Search;
