import React from "react";
import Link from "next/link";

export default class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: this.props.flag,
      name: this.props.name,
      code: this.props.id,
    };
  }

  render() {
    const { list } = this.props;
    const { flag, name, code } = this.state;
    return (
      <Link as={`/countries/${code}`} href="/countries/[code]">
        <div className={`card ${this.props.className}`}>
          <div className="flag">
            <img src={flag} alt={name + " flag"} />
          </div>
          <div className="name">{name}</div>
          <div className="body">
            <ul>
              {list.map((item, index) => (
                <li className="item" key={index}>
                  <span className="label">{item.label}</span>:{" "}
                  <span className="value">{item.value}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </Link>
    );
  }
}
