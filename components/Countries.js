import React, { useState, useEffect } from "react";
import Link from "next/link";

const API_URL = "https://restcountries.eu/rest/v2/all";

const Countries = () => {
  const [countries, setCountries] = useState([]);

  const getCountries = () => {
    fetch(API_URL)
      .then((res) => res.json())
      .then((countries) => {
        setCountries(countries);
        console.log(countries);
      });
  };

  useEffect(() => {
    getCountries();
  }, []);

  return (
    <>
      <div className="container">
        <div className="row flex-wrap mt-5">
          {countries.map((country) => {
            const { name, region, subregion, currencies, flag, alpha3Code } =
              country;

            return (
              <article
                key={alpha3Code}
                className=" mx-auto card cardC col-md-4 mx-2 my-4"
              >
                <Link
                  as={`/country/${alpha3Code}`}
                  href="/country/[alpha3Code]"
                >
                  <div className="link">
                    <img src={flag} />
                    <h3 className="p-2 text-center country-names">{name}</h3>
                    <p>Currency: {currencies[0].name} </p>
                    <p>
                      Region:{" "}
                      <span>
                        {region}, {subregion}
                      </span>
                    </p>
                  </div>
                </Link>
              </article>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Countries;
