import Link from "next/link";

// export default function Header() {
//   return (
//     <div>
//       <header>
//         <div className="container">
//           <Link href="/">Country App</Link>
//         </div>
//       </header>
//     </div>
//   );
// }

export default function Header() {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4 sticky-top">
      <div className="container appName text-light">
        <Link className="navbar-brand" href="/all">
          Country App
        </Link>
      </div>
    </nav>
  );
}
